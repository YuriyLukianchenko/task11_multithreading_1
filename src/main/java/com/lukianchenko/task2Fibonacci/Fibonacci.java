package com.lukianchenko.task2Fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Fibonacci {
    private Logger logger = LogManager.getLogger(Fibonacci.class);

    public Fibonacci(){

    }

    public  Fibonacci(int n, String name){
        int sum = 0;
        int previous = 0;
        int current = 1;
        for(int i = 1; i <= n; i++){
            sum = previous +current;
            previous = current;
            current = sum;
            logger.info(name + " thread, Fibonacci number: " + sum);
        }
         logger.info(name + " thread Finished");

    }

    public int calculate(int n, String name) {
        int sum = 0;
        int fibNum = 0;
        int previous = 0;
        int current = 1;
        for (int i = 1; i <= n; i++) {
            fibNum = previous + current;
            previous = current;
            current = fibNum;
            sum += fibNum;
           // System.out.println(name + " thread, Fibonacci number: " + fibNum);
        }
        System.out.println(name + " thread Finished");
        return sum;
    }
}
