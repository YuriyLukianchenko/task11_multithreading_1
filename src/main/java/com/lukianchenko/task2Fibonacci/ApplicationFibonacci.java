package com.lukianchenko.task2Fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationFibonacci {
    Logger logger = LogManager.getLogger(ApplicationFibonacci.class);

    Thread thread1 = new Thread(()-> new Fibonacci(5, "First").calculate(5, "FirstC"));
    Thread thread2 = new Thread(()-> new Fibonacci(5, "Second").calculate(5, "SecondC"));
    Thread thread3 = new Thread(()-> new Fibonacci(5, "Third").calculate(5, "ThirdC"));

    public static void main(String[] args) {
        ApplicationFibonacci app = new ApplicationFibonacci();
        app.logger.info("start");
        app.thread1.start();
        app.logger.info("after 1");
        app.thread2.start();
        app.logger.info("after 2");
        app.thread3.start();
        app.logger.info("after 3");


        try{
            app.thread1.join();
            app.logger.info("join 1");
            app.thread2.join();
            app.logger.info("join 2");
            app.thread3.join();
            app.logger.info("join 3");
        }
        catch(InterruptedException e) {}

    }
}
