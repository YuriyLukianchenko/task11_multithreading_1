package com.lukianchenko.task2Fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Applicattion2 {
        Logger logger = LogManager.getLogger(com.lukianchenko.task2Fibonacci.ApplicationFibonacci.class);

        Thread thread1 = new Thread(()-> new Fibonacci(10, "First"));
        Thread thread2 = new Thread(()-> new Fibonacci(10, "Second"));
        Thread thread3 = new Thread(()-> new Fibonacci(10, "Third"));

        public static void main(String[] args) {
           Applicattion2 app = new Applicattion2();
            app.logger.info("start");
            app.thread1.start();
            app.logger.info("after 1");
            app.thread2.start();
            app.logger.info("after 2");
            app.thread3.start();
            app.logger.info("after 3");


            try{
                app.thread1.join();
                app.logger.info("join 1");
                app.thread2.join();
                app.logger.info("join 2");
                app.thread3.join();
                app.logger.info("join 3");
            }
            catch(InterruptedException e) {}

        }
    }


