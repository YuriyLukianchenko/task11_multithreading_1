package com.lukianchenko.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application6 {
    Object obj1 =new Object();

    Logger logger = LogManager.getLogger(Application6.class);
    public  void method1(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 1");
        }}
    }
    public void method2(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 2");
        }}
    }
    public void method3(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 3");
        }}
    }

    public static void main(String[] args) {
        Application6 app = new Application6();


        Thread thread1 = new Thread(()-> app.method1());
        Thread thread2 = new Thread(()-> app.method2());
        Thread thread3 = new Thread(()-> app.method3());

        thread1.start();
        thread2.start();
        thread3.start();

        //why its working without join!? why main wait while other threads is being performed
    }
}
