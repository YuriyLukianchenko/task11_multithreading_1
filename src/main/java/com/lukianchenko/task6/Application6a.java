package com.lukianchenko.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application6a {
    Logger logger = LogManager.getLogger(Application6a.class);
    Object obj1 =new Object();

    public  void method1(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 1");
        }}
    }
    public  void method2(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 2");
        }}
    }
    public   void method3(){ synchronized(obj1){
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 3");
        }}
    }

    public static void main(String[] args) {
        Application6a app = new Application6a();
        Application6a app1 = new Application6a();
        Application6a app2 = new Application6a();

        Thread thread1 = new Thread(app::method1);
        Thread thread2 = new Thread(()-> app1.method2());
        Thread thread3 = new Thread(()-> app2.method3());

        thread1.start();
        thread2.start();
        thread3.start();

    }
}