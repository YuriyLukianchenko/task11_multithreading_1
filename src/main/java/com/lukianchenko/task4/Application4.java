package com.lukianchenko.task4;

import com.lukianchenko.task2Fibonacci.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application4 {


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Logger logger = LogManager.getLogger(Application4.class);
        Fibonacci fib1 = new Fibonacci();
        Fibonacci fib2 = new Fibonacci();
        Fibonacci fib3 = new Fibonacci();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        logger.info("Fibonnaci Sum =" + executorService.submit(()->fib1.calculate(50,"first")).get());
        logger.info("Fibonnaci Sum =" + executorService.submit(()->fib2.calculate(5,"second")).get());
        logger.info("Fibonnaci Sum =" + executorService.submit(()->fib3.calculate(10,"third")).get());

        executorService.shutdown();
    }
}
