package com.lukianchenko.task3;


import com.lukianchenko.task2Fibonacci.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application3 {
    Logger logger = LogManager.getLogger(Application3.class);



    public static void main(String[] args) {
        Application3 app = new Application3();
        Thread thread1 = new Thread(()-> new Fibonacci(10, "First"));
        Thread thread2 = new Thread(()-> new Fibonacci(10, "Second"));
        Thread thread3 = new Thread(()-> new Fibonacci(10, "Third"));
        app.logger.info("start");
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(thread1);
        app.logger.info("after 1");
        executor.execute(thread2);
        app.logger.info("after 2");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(thread3);
        app.logger.info("after 3");

        app.logger.info("here0");


        /*
        try{
            thread1.join();
            app.logger.info("join 1");
            thread2.join();
            app.logger.info("join 2");
            thread3.join();
            app.logger.info("join 3");
        }
        catch(InterruptedException e) {}
        */
        app.logger.info("here1");


        executorService.shutdownNow();
        app.logger.info("here3");
        ((ExecutorService) executor).shutdown();

    }
}
