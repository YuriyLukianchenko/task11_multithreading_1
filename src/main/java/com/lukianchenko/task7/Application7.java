package com.lukianchenko.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Application7 {
    public static void main(String[] args) throws IOException, InterruptedException {
        final PipedInputStream pipeIn = new PipedInputStream();
        final PipedOutputStream pipeOut = new PipedOutputStream();

        pipeIn.connect(pipeOut);
        Thread pipeWriter = new Thread(() -> {for(int i = 0; i< 255; i++) {
            try{
                pipeOut.write(i);
                Thread.sleep(100);
            } catch(IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }});
        Thread pipeReader = new Thread(() -> {for(int i = 0; i< 255; i++) {
            try{
                System.out.println((char)pipeIn.read());
                Thread.sleep(200);
            } catch(IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }});

        pipeWriter.start();
        pipeReader.start();

        pipeReader.join();
        pipeWriter.join();

        pipeIn.close();
        pipeOut.close();
    }
}
