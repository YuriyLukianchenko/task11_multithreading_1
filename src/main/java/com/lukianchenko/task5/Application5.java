package com.lukianchenko.task5;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.*;

import static java.lang.Thread.sleep;

public class Application5 {
    public static void main(String[] args) throws InterruptedException {
        Callable<Integer> runner1 = () -> {int i = new Random().nextInt(10);
                sleep(i); return i;};
        Callable<Integer> runner2 = () -> {int i = new Random().nextInt(10);
            sleep(i); return i;};
        Callable<Integer> runner3 = () -> {int i = new Random().nextInt(10);
            sleep(i); return i;};
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);
        executor.invokeAll(Arrays.asList(runner1, runner2, runner3))
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        return 1;
                    })


                .forEach(System.out::println);
        executor.shutdown();

    }
}
