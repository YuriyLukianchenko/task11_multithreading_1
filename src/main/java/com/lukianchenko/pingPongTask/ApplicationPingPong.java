package com.lukianchenko.pingPongTask;

import com.lukianchenko.pingPongTask.PingPong;

public class ApplicationPingPong {
    public static void main(String[] args) {
        PingPong pingpong = new PingPong();
        pingpong.showPingPong();
    }
}
