package com.lukianchenko.pingPongTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;


public class PingPong {
    private Logger logger = LogManager.getLogger(PingPong.class);
    private volatile static long counter = 0;    // shared resource
    private static Object synchro = new Object(); //Monitor

    public void showPingPong() {
        Thread pingThread = new Thread(() -> { synchronized(synchro) {
            for (int i = 1; i <= 3; i++ ) {
                logger.info("before wait ping");
                try {synchro.wait();  logger.info("after wait ping");}
                catch(InterruptedException e){}
                counter++;
                logger.info("Ping " + counter);
                synchro.notify();
            }
            logger.info("Finish " + Thread.currentThread().getName());
        }});
        Thread pongThread = new Thread(() -> {synchronized(synchro){
            for (int i = 1; i <= 3; i++ ) {
                logger.info("before wait pong");
                synchro.notify();
                try {synchro.wait(); logger.info("after wait pong");}
                catch(InterruptedException e){}
                counter++;
                logger.info("Pong " + counter);
            }
            logger.info("Finish " + Thread.currentThread().getName());
        }});
        Thread pungThread = new Thread(){
            @Override
            public void run(){
                synchronized (synchro) {
                    for (int i = 1; i <= 3; i++) {
                        synchro.notify();
                        try {
                            synchro.wait();
                        } catch (InterruptedException e) {
                        }
                        counter++;
                        logger.info("Pung " + counter);
                    }
                }
                logger.info("Finish " + Thread.currentThread().getName());
            }
        };
        Thread pengThread = new Thread(new Peng());

        logger.info(LocalDateTime.now());
        pingThread.start();
        pongThread.start();
        pengThread.start();
        pungThread.start();
        try {
            pingThread.join();pongThread.join();pengThread.join();pungThread.join();
        }
        catch(InterruptedException e) {}
        logger.info(LocalDateTime.now());
        logger.info("counter = " + counter);
    }

    public class Peng implements Runnable {
        @Override
        public void run(){
            synchronized(synchro){
                for (int i = 1; i <= 3; i++ ) {
                    synchro.notify();
                    try {synchro.wait();}
                    catch(InterruptedException e){}
                    counter++;
                    logger.info("Peng " + counter);
                }
                logger.info("Finish " + Thread.currentThread().getName());
            }
        }
    }
}
